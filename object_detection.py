import cv2
import numpy as np
import os

net = cv2.dnn.readNet('yolov3_training_10000.weights', 'yolov3_testing.cfg')
classes = []
with open('classes.txt','r') as f:
    classes = f.read().splitlines()

test_dir = 'images'
test_img = 'bee7_new.jpg'

path = test_dir + '/' + test_img

img = cv2.imread(path)
height, width, _ = img.shape

blob = cv2.dnn.blobFromImage(img, 1/255, (416, 416), (0,0,0), swapRB=True, crop=False)

# for b in blob:
#     for n, img_blob in enumerate(b):
#         cv2.imshow(str(n), img_blob)

net.setInput(blob)

output_layers_names = net.getUnconnectedOutLayersNames()
layerOutputs = net.forward(output_layers_names)

# print("output_layers_names")
# print(output_layers_names)
#
# print("layerOutputs")
# print(layerOutputs)

boxes=[]
confidences = []
class_ids = []

for output in layerOutputs:
    # print("ouput in layerouput")
    # print(output)
    # print("length of output")
    # print(len(output))
    for detection in output:
        # print("detection in output")
        # print(detection)
        scores = detection[5:]
        class_id = np.argmax(scores)
        # print("classid")
        # print(class_id)
        confidence = scores[class_id]
        # print("confidence")
        # print(confidence)
        if confidence > 0.2:
            center_x = int(detection[0]*width)
            center_y = int(detection[1]*height)
            w = int(detection[2]*width)
            h = int(detection[3]*height)

            x = int(center_x - w/2)
            y = int(center_y - h/2)

            boxes.append([x, y, w, h])
            confidences.append((float(confidence)))
            class_ids.append(class_id)

print("boxes")
print(boxes)
print("confidences")
print(confidences)
print("class_ids")
print(class_ids)



print(len(boxes))
indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.2, 0.4)
# print(indexes.flatten())
print(indexes)
font = cv2.FONT_HERSHEY_PLAIN
colors = np.random.uniform(0, 255, size=(len(boxes), 3))

if len(indexes) > 0:
    for i in indexes.flatten():
        #for i in indexes:
        x, y, w, h = boxes[i]
        label = str(classes[class_ids[i]])
        confidence = str(round(confidences[i], 2))
        color = colors[i]
        cv2.rectangle(img, (x,y), (x+w, y+h), color, 2)
        cv2.putText(img, label + " " + confidence, (x, y+20), font, 2, (0, 255, 0), 2)
        # cv2.putText(img, label + " " + confidence, (x, y+20), font, 2, (255, 255, 255), 2)


cv2.imshow('Image', img)
# save_dir = 'detected_imges' + '/'
# cv2.imwrite(os.path.join(save_dir , 'aphid.jpg'), img1)
cv2.waitKey(0)
cv2.destroyAllWindows()
