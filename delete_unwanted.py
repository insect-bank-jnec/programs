
#-------------- First check for .xml files in  labels and remove those -------------------

import os
image_folder_path = 'Tent Caterpillars' + '/' +'images'
label_folder_path = 'Tent Caterpillars' + '/' +'labels'
images = []
labels = []
notlabelled = []
for i in os.listdir(image_folder_path) :
    i = i.split('.', 1)[0]
    images.append(i)
for i in os.listdir(label_folder_path) :
    i = i.split('.', 1)[0]
    labels.append(i)

for i in images :
    if i in labels :
        continue
    else :
        notlabelled.append(i)
        # INFORMATION: 'builtin_function_or_method' object is not subscriptable : if we use [ ] i.e ...append[i]

print(notlabelled)
print(len(notlabelled))

# ------------ Delete ----------------------
count = 0
for i in notlabelled :
    i = "Tent Caterpillars" + "/" + "images" + "/" + i + ".jpg"
    if os.path.exists(i) :
        os.remove(i)
        count += 1
print(count)





