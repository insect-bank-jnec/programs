# Code by: Mansi Ayaskar
# Change the label no./class no. of all files in a directory at a time
import os
path= "labels"
l = []
for f in os.listdir(path):
	with open(path+'/'+f,'r') as f_in:
		for line in f_in:
			if line=="\n":
				continue
			else :
				words = line.split(' ',1)[1]
				# print(words)
				n = "50" +" " + words
				l.append(n)
				# print(n)
		text = ''.join(l)
		open(path+'/'+ f,'w').write(text)
		l.clear()
	
