from keras.preprocessing.image import ImageDataGenerator, load_img, img_to_array
import os
imagepath = "wasp"
images = []
datagen = ImageDataGenerator(
        rotation_range=40,
        width_shift_range=0.3,
        height_shift_range=0.5,
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True,
        fill_mode='nearest')
for img in os.listdir(imagepath):
    images.append(os.path.join(imagepath, img))

for i in images :
    pic = load_img(i)
    X = img_to_array(pic)
    X.shape # Image dimension
    output=(1024, 768, 3)
    X = X.reshape((1,) + X.shape) # Converting into 4 dimension array
    X.shape
    output=(1, 1024, 768, 3)
    # Generate 10 images
    # batch_size: At a time, how many image should be created.
    count = 0
    #number = 1
    n = i
    for batch in datagen.flow(X, batch_size=1, save_to_dir='augumented_wasp', save_prefix= "wasp", save_format='jpeg'):
        count += 1
        #number += 1
        if count == 4:
            break
#​print('10 images have been generated')
